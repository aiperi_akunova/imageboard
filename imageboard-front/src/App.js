import './App.css';
import NewMessage from "./components/NewMessage/NewMessage";
import {useDispatch, useSelector} from "react-redux";
import {createMessage, fetchMessages} from "./store/actions/messagesActions";
import {useEffect} from "react";
import MessageBox from "./components/MessageBox/MessageBox";

const App = () => {
    const dispatch = useDispatch();
    const messages = useSelector(state =>state.messages.messages)
    useEffect(()=>{
            dispatch(fetchMessages());
    },[dispatch])

    const onSubmit = async productData => {
        await dispatch(createMessage(productData));
    };

    return (
        <div className="App">
            <NewMessage onSubmit={onSubmit}/>
            <div className='messages'>
            {messages && messages.map(m=>(
                <MessageBox
                title={m.author}
                message={m.text}
                image={m.image}
                />
            ))}
            </div>
        </div>
    );
};

export default App;
