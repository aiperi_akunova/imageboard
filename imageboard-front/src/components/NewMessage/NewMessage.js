import React, {useState} from 'react';
import './NewMessage.css';
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/messagesActions";

const NewMessage = ({onSubmit}) => {

    const dispatch = useDispatch();

    const [state, setState] = useState({
        text: "",
        author: "",
        image: null,
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e =>{
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file}
        })
    }

    // const onSubmit = async productData => {
    //     await dispatch(createMessage(productData));
    //     // history.replace('/');
    // };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        // if(state.author !== ''){
        //     formData.append('author', state.author);
        // } else{
        //     formData.append('author', 'anonymous');
        // }

        formData.append('author', state.author);
        formData.append('text', state.text);
        formData.append('image', state.image);

        onSubmit(formData);
    };

    return (
        <>
            <div className='input'>
                <label>
                    Message:
                    <input
                        required
                        autoComplete='off'
                        type='text'
                        value={state.text}
                        name='text'
                        onChange={inputChangeHandler}/>
                </label>

                <label>
                    Author:
                    <input
                        type='text'
                        autoComplete='off'
                        value={state.author}
                        name='author'
                        onChange={inputChangeHandler}/>
                </label>

                <label>
                    Image:
                    <input
                        autoComplete='off'
                        type='file'
                        name='image'
                        onChange={fileChangeHandler}/>
                </label>

                <button type='submit' onClick={submitFormHandler}>Post</button>
            </div>

        </>
    );
};

export default NewMessage;