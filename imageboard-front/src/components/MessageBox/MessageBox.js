import React from 'react';
import './MessageBox.css';
import {apiUrl} from "../../config";

const MessageBox = props => {

    let cardImage;
    if(props.image){
        cardImage = apiUrl + '/uploads/'+props.image;
    }

    return (
        <div className='box'>
            <h3>Author: {props.title}</h3>
            <p> <b>Message: </b>{props.message}</p>
            {props.image && <img src={cardImage} alt="Card"/>
            }
        </div>
    );
};

export default MessageBox;