const express = require('express');
const fileDb = require('../fileDb');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb)=>{
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
  const messages = fileDb.getItems();
  res.send(messages);
});

// router.get('/:id', (req, res) => {
//   const product = fileDb.getItem(req.params.id);
//   if (!product) {
//     return res.status(404).send({error: 'Product not found'});
//   }
//
//   res.send(product);
// });

router.post('/', upload.single('image'), (req, res) => {
  if (!req.body.text) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const message = {
    text: req.body.text,
    author: req.body.author,
  }

  if(req.file){
    message.image = req.file.filename;
  }

  const newMessage = fileDb.addItem(message);

  res.send(newMessage);
});

module.exports = router; // export default router;